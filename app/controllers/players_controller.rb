class PlayersController < ApplicationController
  before_action :set_player, only: [:show, :edit, :update, :destroy]

  # GET /players
  # GET /players.json
  def index



    @players = Player.order(wins: :desc, losses: :asc)
  end

  # GET /players/1
  # GET /players/1.json
  def show
    respond_to :json
  end

  # GET /players/new
  def new
    @player = Player.new
  end

  # POST /players
  # POST /players.json
  def create
    @player = Player.new(player_params)

    respond_to do |format|
      if @player.save
        format.html { redirect_to players_path(player_action: 'added', player_id: @player.id), notice: 'Player added.' }
        format.json { render :show, status: :created, location: @player }
      else
        format.html { render :new }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /players/1
  # PATCH/PUT /players/1.json
  def update
    respond_to do |format|
      if @player.mark_win
        format.html { redirect_to players_path(player_action: 'win', player_id: @player.id) }
        format.json { render :show, status: :ok, location: @player }
      else
        format.html { render players_path, alert: 'There was an error' }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /players/1
  # DELETE /players/1.json
  def destroy
    respond_to do |format|
      if @player.mark_loss
        format.html { redirect_to players_path(player_action: 'loss', player_id: @player.id) }
        format.json { render :show, status: :ok, location: @player }
      else
        format.html { render players_path, alert: 'There was an error' }
        format.json { render json: @player.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_player
      @player = Player.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def player_params
      params.require(:player).permit(:name, :wins, :losses)
    end
end
