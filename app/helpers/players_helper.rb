module PlayersHelper

  def player_class(player)
    return unless params[:player_id].to_i == player.id

    case params[:player_action]
    when 'added'
      'player--added'
    when 'win'
      'player--win'
    when 'loss'
      'player--loss'
    else
      nil
    end
  end
end
