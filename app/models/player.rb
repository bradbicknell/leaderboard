class Player < ActiveRecord::Base

  validates_numericality_of :wins, only_integer: true, greater_than_or_equal_to: 0
  validates_numericality_of :losses, only_integer: true, greater_than_or_equal_to: 0


  def mark_win
    self.update(wins: self.wins + 1)
  end

  def mark_loss
    self.update(losses: self.losses + 1)
  end

  def win_ratio
    return nil if total_games == 0

    wins / total_games.to_f
  end

  def total_games
    losses + wins
  end

end
