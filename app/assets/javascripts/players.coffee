# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


jQuery ->
  $(document).on 'page:update', ()->
    $('.player--win').removeClass('player--win')
    $('.player--loss').removeClass('player--loss')
    $('.player--added').removeClass('player--added')
